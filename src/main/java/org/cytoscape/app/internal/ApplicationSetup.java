/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cytoscape.app.internal;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.yaml.snakeyaml.Yaml;

/**
 *
 * @author sergeyn
 */
public class ApplicationSetup {

    protected static final String COLUMN_DELIMETER = " ";
    protected static final String ARRAY_DELIMETER = ",";

    private String appCaption; //show this in menu

    private boolean isBatchMode; //type: cmd | script    
    private String appContext; //opens for: node | edge

    private String dataMatrixFile;
    private String innerFileName;
    private String columnDelimiter;
    private String arrayDelimiter;

    private String binaryPathToRun; //the binary to execute
    private String cmdLineArgs; //command line arguments

    //relevant only for batch
    private String scriptLines;
    private String suffix;

    private String dir;
    private Transformer transformer;

    public String[] node(String nodeLabel) {
        return transformer.node(nodeLabel,dir);
    }

    public String[] edge(String edgeLabel, String node1, String node2) {
        return transformer.edge(edgeLabel, node1, node2,dir);
    }

    private ApplicationSetup() {
    }

    public void setDir(String dir) {
        this.dir = dir;
    }
    public String getSuffix() {
        return suffix;
    }

    public String getColumnDelimiter() {
        return columnDelimiter;
    }

    public String getArrayDelimiter() {
        return arrayDelimiter;
    }

    public String getScriptLines() {
        return scriptLines;
    }

    public String getDataMatrixFile() {
        return dataMatrixFile;
    }
    
    public String getInnerFile() {
        return innerFileName;
    }

    public String getCmdLine() {
        return cmdLineArgs;
    }

    public String getAppContext() {
        return appContext;
    }

    public boolean isForNode() {
        assert (appContext.equals("node") || appContext.equals("edge"));
        return appContext.equals("node");
    }

    public boolean isForEdge() {
        assert (appContext.equals("node") || appContext.equals("edge"));
        return appContext.equals("edge");
    }

    public String getAppCaption() {
        return appCaption;
    }

    public String getBinaryPathToRun() {
        return binaryPathToRun;
    }

    public boolean isBatchMode() {
        return isBatchMode;
    }
    //public String getRawCmds() {      return rawCmds;    }

    public void readFromFile(String fname) {
    }

    static public List<ApplicationSetup> getApps(ArrayList<String> rawAppData) {
        Yaml yaml = new Yaml();
        String raw = Utils.joinArrayString(rawAppData);
        List<ApplicationSetup> apps = new ArrayList<ApplicationSetup>();

        Iterable<Object> datas = null;
        try {
            datas = yaml.loadAll(raw);
        } catch (Exception ex) {
        }
        if (null == datas) {
            ErrBuffer.err("Could not parse YAML.\nPlease check your input with an online validator at http://yamllint.com/");
        }
 try {
        for (Object data : datas) {
            Map mData = (Map) data;

            ApplicationSetup setup = new ApplicationSetup();

            setup.appCaption = Utils.safeGetString(mData, "appCaption");
            setup.isBatchMode = Utils.safeGetString(mData, "type").equals("script");
            setup.appContext = Utils.safeGetString(mData, "appContext");
            setup.dataMatrixFile = Utils.safeGetString(mData, "dataMatrixFile");
            setup.innerFileName = Utils.safeGetString(mData, "innerFileName");
            setup.arrayDelimiter = Utils.safeGetString(mData, "arrayDelimiter", ARRAY_DELIMETER);
            setup.columnDelimiter = Utils.safeGetString(mData, "columnDelimiter", COLUMN_DELIMETER);

            setup.binaryPathToRun = Utils.safeGetString(mData, "appBinary");
            setup.cmdLineArgs = Utils.safeGetString(mData, "cmdLineArgs");
            setup.scriptLines = Utils.safeGetString(mData, "scriptLines");
            setup.suffix = Utils.safeGetString(mData, "suffix");
            setup.transformer = new Transformer(setup);
            setup.dir = "";
            apps.add(setup);
            ErrBuffer.out(setup.appCaption+" for "+setup.appContext);
        }
 } catch (Exception ex) {
      ErrBuffer.err("Could not parse YAML.\nPlease check your input with an online validator at http://yamllint.com/");
        }        
        return apps;
    }
}
