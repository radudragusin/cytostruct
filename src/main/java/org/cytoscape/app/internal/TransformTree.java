package org.cytoscape.app.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author sergeyn
 */
public class TransformTree {

    protected String text;
    protected Boolean isRawVorH; //null for raw, false for V, true for H
    protected ArrayList<TransformTree> children;

    public static class DataPackage {

        public StringBuilder buf;
        public ArrayList<String[]> dataMatrix;
        public Map<String, Integer> headerMap;
        public String arrayDelimiter;
    }

    @Override
    public String toString() {
        return isRawVorH.toString();
    }

    public TransformTree(String t, Boolean isRVH) {
        text = t;
        isRawVorH = isRVH;
        children = new ArrayList<TransformTree>();
    }

    protected HashMap<String, String[]> getArrays(DataPackage dp, int row) {
        HashMap<String, String[]> arrays = new HashMap<String, String[]>();
        if(dp.headerMap == null || dp.headerMap.isEmpty())
            return arrays;
        for (String column : dp.headerMap.keySet()) { //scan columns for arrays present in block
            if (text.contains(column)) {
                String rawValueForKey = dp.dataMatrix.get(row)[dp.headerMap.get(column)];
                String[] values = rawValueForKey.split(dp.arrayDelimiter);
                if (values.length > 1) { //an internal array
                    arrays.put(column, values);
                }
            }
        }
        return arrays;
    }

    protected void rawTransform(DataPackage dp, int row, int col) {
        StringBuilder buf = new StringBuilder();
        HashMap<String, String[]> arrays = getArrays(dp, row);

        if (!arrays.isEmpty()) { //first handle arrays unwinding:
//this code is not relevant now
//            int arraySz = arrays.values().iterator().next().length;
//            for (String column : arrays.keySet()) {
//                if (arrays.get(column).length != arraySz) {
//                    ErrBuffer.err("Sizes of arrays differ while unwinding!");
//                    return;
//                }
//            }
            String blockToReplace = text;
            if (col == -1) { //raw -- don't unwind at all
                //for (int i = 0; i < arraySz; ++i) {
                for (String column : arrays.keySet()) {
                    blockToReplace = blockToReplace.replace("%" + column + "%", dp.dataMatrix.get(row)[dp.headerMap.get(column)]); //get the entire array
                }
                buf.append(blockToReplace);
                //} //end for all dimensions of the arrays
            } //end unwind all
            else {
                for (String column : arrays.keySet()) {
                    blockToReplace = blockToReplace.replace("%" + column + "%", arrays.get(column)[col]);
                }
                buf.append(blockToReplace);
            }
        } //end if there are arrays at all

        String firstStage = (buf.length() > 0) ? buf.toString() : text;
        if(dp.headerMap != null && dp.headerMap.isEmpty() == false)
            for (String column : dp.headerMap.keySet()) {
                if (firstStage.contains(column)) {
                    String rawValueForKey = dp.dataMatrix.get(row)[dp.headerMap.get(column)];
                    String[] values = rawValueForKey.split(dp.arrayDelimiter);
                    if (values.length == 1) { //not an array
                        firstStage = firstStage.replace("%" + column + "%", values[0]);
                    }
                }
            }
        dp.buf.append(firstStage);
    }

    public void transformRoot(DataPackage dp) {
        for (TransformTree t : children) {
            t.transform(dp, 0, -1);
        }
    }

    public void transform(DataPackage dp, int row, int col) {
        if (isRawVorH == null) { //raw
            rawTransform(dp, row, col);
        } else if (isRawVorH == false) { //vertical
            for (int i = 0; i < dp.dataMatrix.size(); ++i) {
                for (TransformTree t : children) {
                    t.transform(dp, i, col);
                }
            }
        } else if (isRawVorH == true) { //horizontal
            int arraySz = -1;
            for (TransformTree t : children) {
                if (!t.getArrays(dp, row).isEmpty()) {
                    arraySz = t.getArrays(dp, row).values().iterator().next().length;
                    break;
                }
            }
            if (-1 != arraySz) { //first handle arrays unwinding:
                for (int i = 0; i < arraySz; ++i) {
                    for (TransformTree t : children) {
                        t.transform(dp, row, i);
                    }
                }
            } // end if had arrays
            else {
                for (TransformTree t : children) {
                    t.transform(dp, row, col);
                }
            }

        }
    }

    public void addChild(TransformTree newT) {
        children.add(newT);
    }
}
