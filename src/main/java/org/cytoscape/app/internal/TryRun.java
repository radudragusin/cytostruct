//for a more robust solution check
//from http://www.java2s.com/Tutorial/Java/0120__Development/Helpermethodtoexecuteshellcommand.htm
package org.cytoscape.app.internal;

import java.io.File;
import java.io.IOException;


/*
 * Unix
 */
class TryRun {

    static Process exec(String[] cmdarray) throws IOException {
        if (isWindows()) {
            return execWindows(cmdarray);
        }
        return execUnix(cmdarray);

    }

    static Process execUnix(String[] cmdarray) throws IOException {
        // instead of calling command directly, we'll call the shell
        Runtime rt = Runtime.getRuntime();
        File rDir = new File(System.getProperty("user.dir")).getAbsoluteFile();
        String cmd = cmdarray[0] + " " + cmdarray[1]; //should be exactly 2 elements
        String path = Utils.writeToTempAndGetPath("cd " + rDir.getAbsolutePath() + " \n "+cmd, "run_", "sh");
        if (path.isEmpty()) {
            ErrBuffer.err("Could not produce script");
            return null;
        }
        String[] cmdA = {"sh", path};
        String[] env = {};

        ErrBuffer.out("executing: " + cmdA[0] + " " + cmdA[1] + " @" +  rDir.getAbsolutePath());
        return rt.exec(cmdA);//,rDir);
    }

    static String escapeQuote(String s) {
        // replace single quotes with a bit of magic (end-quote, escaped-quote, start-quote) 
        // that works in a single-quoted string in the Unix shell
        if (s.indexOf('\'') != -1) {
            s = s.replace("'", "'\\''");
        }
        return s;
    }

    /*
     * Windows
     */
    static boolean isWindows() {
        String os = System.getProperty("os.name");
        return (os != null && os.startsWith("Windows"));
    }

    static Process execWindowsOld(String[] cmdarray) throws IOException {
        return Runtime.getRuntime().exec(cmdarray);
    }

    static Process execWindows(String[] cmdarray) throws IOException {
        String cmd = cmdarray[0] + " " + cmdarray[1]; //should be exactly 2 elements
        File rDir = new File(System.getProperty("user.dir")).getAbsoluteFile();
        ProcessBuilder pb = new ProcessBuilder("cmd", "/C",cmdarray[0], cmdarray[1]);	
        pb.directory(rDir);
        
//        String batF = Utils.writeToTempAndGetPath("cd "+rDir.getAbsolutePath()+"\n"+cmd, "run_", "bat");
        ErrBuffer.out("executing: " + cmd +  " @ " + rDir.getAbsolutePath());    
        
     //   String[] env = {};
       // return Runtime.getRuntime().exec(cmd,env,rDir); //
        return pb.start();
    }

    static boolean linked = false; // true after System.loadLibrary() is called
}
