/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cytoscape.app.internal;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 *
 * @author sergeyn
 */
public class Transformer {

    final private ApplicationSetup application;
    private Map<String, ArrayList<String[]>> labelToData;
    private Map<String, Integer> headerMap;
    private TransformTree transformTree;

    final protected String splittingRegex = "(?<=%V_START%|%V_END%|%H_START%|%H_END%)|(?=%V_START%|%V_END%|%H_START%|%H_END%)";
    final protected static String TAG_ST = "_START%";
    final protected static String TAG_EN = "_END%";
    final protected static String H_START = "%H" + TAG_ST;
    final protected static String H_END = "%H" + TAG_EN;
    final protected static String V_START = "%V" + TAG_ST;
    final protected static String V_END = "%V" + TAG_EN;

    public Transformer(ApplicationSetup app) {
        application = app;
        transformTree = null;
        tryAndLoadDataMatrix();        
    }
    
    private static String getExt(String fileName) {
        int i = fileName.lastIndexOf('.');
        return (i > 0)  ? fileName.substring(i+1) : "";
    }

    private void tryAndLoadDataMatrix() {
        labelToData = new HashMap<String, ArrayList<String[]>>();
        String fname;
        if ((fname = application.getDataMatrixFile()).isEmpty()) {
            return;
        }
        File dtm = new File(new File(fname).getAbsolutePath());
        if (!dtm.canRead()) {
            ErrBuffer.err("couldn't read dataMatrixFile: " + dtm.getAbsolutePath());
            return;
        }
        ArrayList<String> allLines;
        String ext =  getExt(fname);
        if(ext.contains("zip"))  {
            allLines = Utils.readAllLinesZip(dtm, application.getInnerFile());
        }
        else if(ext.contains("gz")) {
            allLines = Utils.readAllLinesGz(dtm);
        }
        else {
            allLines = Utils.readAllLines(dtm);
        }
        if(allLines.isEmpty()) {
            ErrBuffer.err("couldn't read dataMatrixFile: " + fname + " " + application.getInnerFile());
            return;
        }
        
        //get header
        String[] header = allLines.get(0).split(application.getColumnDelimiter());
        headerMap = new HashMap<String, Integer>();
        for (int c = 0; c < header.length; ++c) {
            headerMap.put(header[c], c);
        }

        //the rest of it
        for (int i = 1; i < allLines.size(); ++i) { //each line
            String[] columns = allLines.get(i).split(application.getColumnDelimiter());
            if (columns.length != header.length) {
                ErrBuffer.err("columns.length != header.length");
                return;
            }
            if (!labelToData.containsKey(columns[0])) { //if first time
                labelToData.put(columns[0], new ArrayList<String[]>());
            }
            labelToData.get(columns[0]).add(columns);
        }
        ErrBuffer.out("Loading data for <" + application.getAppCaption()
                + "> DataMatrix: " + application.getDataMatrixFile()
                + " Rows: " + labelToData.size()
                + " Columns: " + header.length);
    }

    protected static ArrayList<String[]> getMatrix(Map<String, ArrayList<String[]>> dataMatrix, String label) {
        if (!dataMatrix.containsKey(label)) {
            //ErrBuffer.out("no data in matrix for label: " + label);
            return new ArrayList<String[]>();
        }
        return dataMatrix.get(label);
    }

    protected boolean checkBlocksNestingValid(String[] blocks) {
        Stack<String> stack = new Stack<String>();
        for (String s : blocks) {
            if (s.equals(H_START) || s.equals(V_START)) {
                stack.push(s);
            }
            if (s.equals(H_END) || s.equals(V_END)) {
                if (stack.empty()) {
                    return false;
                }
                if (s.equals(H_END) && !stack.peek().equals(H_START)) {
                    return false;
                }
                if (s.equals(V_END) && !stack.peek().equals(V_START)) {
                    return false;
                }
                stack.pop();
            }
        }
        return stack.empty();
    }

    //null for raw, false for V, true for H
    protected Boolean getTypeBlock(String str) {
        if (str.equals(H_START)) {
            return true;
        }
        if (str.equals(V_START)) {
            return false;
        }
        return null;
    }

    protected TransformTree buildBlocksTree(String[] blocks) {
        Stack<TransformTree> stack = new Stack<TransformTree>();
        //set the root:
        TransformTree root = new TransformTree("root", null);
        stack.push(root);
        for (String s : blocks) {
            if (s.equals(H_END) || s.equals(V_END)) {
                //out("pop " + s);
                stack.pop();
            } else {
                Boolean nextType = getTypeBlock(s);
                TransformTree lastCh = new TransformTree(s, nextType);
                stack.peek().addChild(lastCh);
                if (nextType != null) { //hstart or vstart
                    //out("push " + s);
                    stack.push(lastCh);
                }
            }
        }
        if (1 < stack.size()) {
            ErrBuffer.err("Something is so wrong... stack is not empty after building block tree");
        }
        return root;
    }

    
    final protected void initTransofrmTree() {
         if (!application.isBatchMode()) {
            return;
        }

        String script = application.getScriptLines();
        String[] basicBlocks = script.split(splittingRegex); //get the blocks
//        for(int i = 0; i<basicBlocks.length; ++i) { //clean extra \n
//            if(basicBlocks[i].startsWith(System.getProperty("line.separator"))) 
//                    basicBlocks[i] = basicBlocks[i].replaceFirst(System.getProperty("line.separator"), "");
//        }
        
        if (!checkBlocksNestingValid(basicBlocks)) {
            ErrBuffer.err("**** Error: bad nesting of start/end tags");
            return;
        }

        transformTree = buildBlocksTree(basicBlocks);
       
    }
    
    protected String buildScript(Map<String, String> localBinds, String label) {
        if (!application.isBatchMode()) {
            return "";
        }
        
        TransformTree.DataPackage dp = new TransformTree.DataPackage();
        dp.arrayDelimiter = application.getArrayDelimiter();
        dp.buf = new StringBuilder();
        dp.dataMatrix = getMatrix(labelToData, label);
        dp.headerMap = headerMap;
        
        if(null == transformTree)
            initTransofrmTree();
        if(null == transformTree) return "";
              
        transformTree.transformRoot(dp);

        //write this script to tmp file and return path
        File f = Utils.getTmpFile(label, application.getSuffix());
        FileWriter bw;
        try {
            bw = new FileWriter(f);
            String finalStr = dp.buf.toString();
            //always safe to replace localBinds
            for (String key : localBinds.keySet()) {
                finalStr = finalStr.replace(key, localBinds.get(key));
            }
            bw.write(finalStr);
            bw.flush();
            bw.close();
        } catch (IOException ex) {
           ErrBuffer.err("Problems while writing script to " + f.getAbsolutePath());
        }
        return f.getAbsolutePath();
    }

    protected String getCmd(Map<String, String> localBinds) {
        String cmd = application.getBinaryPathToRun() + " " + application.getCmdLine();
        for (String key : localBinds.keySet()) {
            cmd = cmd.replace(key, localBinds.get(key));
        }
        return cmd;
    }

    public String[] node(String nodeLabel, String dir) {
        Map<String, String> locals = new HashMap<String, String>();
        locals.put("%dir%",dir);
        locals.put("%node%", nodeLabel);
        String[] ret = {getCmd(locals), buildScript(locals, nodeLabel)};
        return ret;
    }

    public String[] edge(String edgeLabel, String node1, String node2, String dir) {
        Map<String, String> locals = new HashMap<String, String>();
        locals.put("%dir%",dir);
        locals.put("%edge%", edgeLabel);
        locals.put("%node1%", node1);
        locals.put("%node2%", node2);
        String[] ret = {getCmd(locals), buildScript(locals, edgeLabel)};
        return ret;
    }
}
