/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cytoscape.app.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 *
 * @author sergeyn
 */
public class GenericRunner implements ExternalRunner {
    
    private Map captionsToNodeApps;
    private Map captionsToEdgeApps;
    
    public GenericRunner() {
    }
    
    public final Map getMapNodes() { return captionsToNodeApps; }
    public final Map getMapEdges() { return captionsToEdgeApps; }
    
    public void init(ArrayList<String> raw) {
        captionsToNodeApps = new HashMap();
        captionsToEdgeApps  = new HashMap();
        
        for(ApplicationSetup app : ApplicationSetup.getApps(raw)) {
            if(app.getAppContext().contains("edge"))
                captionsToEdgeApps.put(app.getAppCaption(), app);
            else if (app.getAppContext().contains("node"))
                captionsToNodeApps.put(app.getAppCaption(), app);
            else
               ErrBuffer.err("No app context for: " + app.getAppCaption());  
        }
    }
    
    protected void tryRun(String[] cmd) {
        try {
            Thread.sleep(10);
            Process p = TryRun.exec(cmd); //Runtime.getRuntime().exec(cmdA);
        } catch (InterruptedException ex) {
            JOptionPane.showMessageDialog(null,"Exception while running external program!\nNote that java could be confused by quotation marks in cmd line");
            ErrBuffer.err(ex.getMessage());
        }
        catch (IOException ex) {
           JOptionPane.showMessageDialog(null,"Exception while running external program!\nNote that java could be confused by quotation marks in cmd line");
           ErrBuffer.err(ex.getMessage());
        }
        
    }
            
    @Override
    public void execNode(String app, String nodeLabel) {
        if (!captionsToNodeApps.containsKey(app)) { //should never happen
            JOptionPane.showMessageDialog(null, "Wrong (caption -> app) mapping for " + app);
            return;
        }

        ApplicationSetup application = (ApplicationSetup) captionsToNodeApps.get(app);
        if (!application.isForNode()) { //should never happen
            JOptionPane.showMessageDialog(null, app + " was declared as an edge app, not a node app");
            return;
        }

        String[] cmd = application.node(nodeLabel);
        tryRun(cmd);

    }
    @Override
    public void execEdge(String app, String edgeLabel, String left, String right){
        if(! captionsToEdgeApps.containsKey(app)){ //should never happen
              JOptionPane.showMessageDialog(null, "Wrong (caption -> app) mapping for " + app);
            return;
        }
        ApplicationSetup application = (ApplicationSetup) captionsToEdgeApps.get(app);
        if (application.isForNode()) { //should never happen
            JOptionPane.showMessageDialog(null, app + " was declared as a node app, not an edge app");
            return;
        }

        String[] cmd = application.edge(edgeLabel,left,right);
        tryRun(cmd);
    }


}
