/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cytoscape.app.internal;

/**
 *
 * @author sergeyn
 */
public interface ExternalRunner {
    void execNode(String app, String nodeLabel);
    void execEdge(String app, String edgeLabel, String left, String right);
}
