package org.cytoscape.app.internal;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CyEdgeViewContextMenuFactory;
import org.cytoscape.application.swing.CyMenuItem;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTableUtil;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;

public class  EdgeViewContextMenuBase implements CyEdgeViewContextMenuFactory, ActionListener {

    private final ExternalRunner runner;
    private final ConfigManager dataRoot;
    private final String caption;

    public EdgeViewContextMenuBase(ConfigManager dataRoot, ExternalRunner runner, String label) {
        super();
        this.dataRoot = dataRoot;
        this.caption = label;
        this.runner = runner;
    }

    @Override
    public CyMenuItem createMenuItem(CyNetworkView netView, View<CyEdge> nodeView) {
        JMenuItem menuItem = new JMenuItem(this.caption);
        menuItem.addActionListener(this);
        CyMenuItem cyMenuItem = new CyMenuItem(menuItem, 0);
        return cyMenuItem;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //Get the selected edges
        final CyApplicationManager manager = dataRoot.cyApplicationManager;        
        List<CyEdge> edges = CyTableUtil.getEdgesInState(manager.getCurrentNetwork(), "selected", true);
        if (edges.isEmpty()) { return;  }
        if (edges.size() > 1) {
            JOptionPane.showMessageDialog(null, "Multiple selection is not supported. Please refine to single node");
            return;
        }

        final CyNetworkView networkView = manager.getCurrentNetworkView();
        final CyNetwork network = networkView.getModel();
        for (CyEdge ed : edges) {
            String edgeLabel = network.getRow(ed).getRaw("interaction").toString();
            String node1Label = network.getRow(ed.getSource()).getRaw(CyNetwork.NAME).toString();
            String node2Label = network.getRow(ed.getTarget()).getRaw(CyNetwork.NAME).toString();
            //String edgeFullName = network.getRow(ed).getRaw("name").toString();
            
            runner.execEdge(this.caption, edgeLabel,node1Label,node2Label);
            break;
        }
        ErrBuffer.showIfErr();
//        if(ErrBuffer.hasStuff()) {
//            JOptionPane.showMessageDialog(null,"Errors while handling an edge\n"+ErrBuffer.getLog());
//        }
    }

}
