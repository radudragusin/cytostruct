/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cytoscape.app.internal;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author sergeyn
 */
public class ErrBuffer {
    private static StringBuilder errBuffer = new StringBuilder();
    private static StringBuilder outBuffer = new StringBuilder();
    private static boolean flag = false;
    
    public static void err(String s) { 
        errBuffer.append((new Date()).toString());
        errBuffer.append("\n");
        errBuffer.append(s);
        errBuffer.append("\n");
        System.err.println(s);
        flag = true;
    }
    
    public static void out(String s ){
       outBuffer.append(s);
       outBuffer.append("\n");
       System.out.println(s);
    }
    
    public static void clear() {
        errBuffer  = new StringBuilder();
        flag = false;
    }
    
    public static String getLog() {
        flag = false;
        errBuffer.append("--------------------\n");
        return errBuffer.toString();
    }
    
    public static String getDebug() {
        return outBuffer.toString();
    }
    
    public static boolean hasStuff() {
        return flag;
    }
    
    
    static public class ErrDialog extends JFrame {

    private JTextArea tLog;

    public ErrDialog(String lines) {
        super("Erors console");

        setSize(new Dimension(220, 40));
        JPanel cfgPanel = new JPanel();
        cfgPanel.setLayout(new BoxLayout(cfgPanel, BoxLayout.Y_AXIS));

        JLabel label = new JLabel("\nErrors were reported during the execution of last actions!\n");
        label.setFont(new Font("Courier", Font.PLAIN, 18));
        tLog = new JTextArea(20, 75);
        tLog.setFont(new Font("Courier", Font.PLAIN, 18));
        tLog.setText(lines);
        tLog.setEditable(false);
        cfgPanel.add(label);
        cfgPanel.add(new JScrollPane(tLog));
        JButton b1 = new JButton("Close");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) { setVisible(false);dispose(); }});

        
        JButton b2 = new JButton("Clear & close");
        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) { ErrBuffer.clear(); setVisible(false);dispose(); }});

        cfgPanel.add(b2);
        cfgPanel.add(b1);


        getContentPane().add(cfgPanel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    } 
    
    
    static public class OutDialog extends JFrame {

    private JTextArea tLog;

    public OutDialog(String lines) {
        super("Debug console");

        setSize(new Dimension(220, 40));
        JPanel cfgPanel = new JPanel();
        cfgPanel.setLayout(new BoxLayout(cfgPanel, BoxLayout.Y_AXIS));

        tLog = new JTextArea(20, 75);
        tLog.setFont(new Font("Courier", Font.PLAIN, 18));
        tLog.setText(lines);
        tLog.setEditable(false);
        cfgPanel.add(new JScrollPane(tLog));
        JButton b1 = new JButton("Close");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) { setVisible(false);dispose(); }});
        cfgPanel.add(b1);
        getContentPane().add(cfgPanel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    } 
    
    public static void showIfErr() {
        if(ErrBuffer.hasStuff())   {
            new ErrBuffer.ErrDialog(ErrBuffer.getLog());
        }
    }
 
}