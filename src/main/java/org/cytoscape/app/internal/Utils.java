/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cytoscape.app.internal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import javax.swing.JOptionPane;

/**
 *
 * @author sergeyn
 */
public class Utils {

    public static File getTmpFile(String prefix, String suffix) {
        File dr = new File(System.getProperty("java.io.tmpdir"), "cytoTmpScripts");
        if (dr.exists() || dr.mkdir()) {
            try {
                return File.createTempFile(prefix+"_scr_", "." + suffix, dr);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Could not work with tmp dir: " + dr.getAbsolutePath());
            }
        }
        return null;
    }

    public static String writeToTempAndGetPath(String text, String prefix, String suffix) {
        File out = getTmpFile(prefix, suffix);
        if (out == null) {
            return "";
        }

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(out));
            writer.write(text);
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            ErrBuffer.err("writeToTempAndGetPath failed");
        }
        return out.getAbsolutePath();
    }

    public static String testNPrint(String s) {
        if (s == null) {
            JOptionPane.showMessageDialog(null, "Empty key :(");
        }
        return s;
    }

    public static String nullEmpty(String s) {
        return (s == null || s.length() == 0) ? null : s;
    }

    static public String safeGetString(Map m, String key) {
        return safeGetString(m, key, "");
    }

    static public String safeGetString(Map m, String key, String defaultV) {
        return (m.containsKey(key)) ? m.get(key).toString() : defaultV;
    }

    static public String joinArrayString(ArrayList<String> arr, int start, int end) {
        StringBuilder buffer = new StringBuilder();
        for (int i = start; i < end; ++i) {
            buffer.append(arr.get(i));
            buffer.append(System.getProperty("line.separator"));
        }
        return buffer.toString();
    }

    static public String joinArrayString(ArrayList<String> arr) {
        StringBuilder buffer = new StringBuilder();
        for (String str : arr) {
            buffer.append(str);
            buffer.append(System.getProperty("line.separator"));
        }
        return buffer.toString();
    }

    static public ArrayList<String> auxReadAllLines(InputStreamReader streamR, String fname) {
        String line;
        ArrayList<String> lines = new ArrayList<String>();
        BufferedReader in;

        try {
            in = new BufferedReader(streamR);
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException ex) {
            ErrBuffer.err("Could not read from file " + fname);
        }
        return lines;

    }

    static public ArrayList<String> readAllLinesGz(File f) {
        try {
            InputStreamReader in = new InputStreamReader(new GZIPInputStream(new FileInputStream(f)));
            return auxReadAllLines(in, f.getAbsolutePath());
        } catch (IOException ex) {
            ErrBuffer.err("Could not read from gzipped file " + f.getAbsolutePath());
        }

        return new ArrayList<String>();
    }

    static public ArrayList<String> readAllLinesZip(File f, String innerFile) {
        try {
            // open the zip file stream
            InputStream theFile = new FileInputStream(f.getAbsoluteFile());
            ZipInputStream stream = new ZipInputStream(theFile);
            ZipEntry entry = null;
            while (null != (entry = stream.getNextEntry())) {
                if (entry.getName().equals(innerFile)) //a match!
                {
                    return auxReadAllLines(new InputStreamReader(stream), f.getAbsolutePath() + " / " + innerFile);
                }
            }
        } catch (IOException ex) {
            ErrBuffer.err("Could not read from file " + f.getAbsolutePath() + " / " + innerFile);
        }

        return new ArrayList<String>();
    }

    static public ArrayList<String> readAllLinesZip(File f, int entryNum) {
        try {
            // open the zip file stream
            InputStream theFile = new FileInputStream(f.getAbsoluteFile());
            ZipInputStream stream = new ZipInputStream(theFile);
            ZipEntry entry;
            for(int i=0; i<entryNum; ++i) {
                entry = stream.getNextEntry();
                        if(null == entry)
                            return new ArrayList<String>();
            }
            return auxReadAllLines(new InputStreamReader(stream), f.getAbsolutePath() + " @ " + entryNum);
        } catch (IOException ex) {
            ErrBuffer.err("Could not read from file " + f.getAbsolutePath() + " @ " + entryNum);
        }
        return new ArrayList<String>();
    }

    static public ArrayList<String> readAllLines(File f) {
        InputStreamReader in;
        try {
            in = new InputStreamReader(new FileInputStream(f));
            return auxReadAllLines(in, f.getAbsolutePath());
        } catch (FileNotFoundException ex) {
            ErrBuffer.err("Could not read from " + f.getAbsolutePath());
        }

        return new ArrayList<String>();
    }

    public static ArrayList<String> StringToLines(String str) {
        ArrayList<String> lines = new ArrayList<String>();
        try {
            BufferedReader rdr = new BufferedReader(new StringReader(str));
            for (String line = rdr.readLine(); line != null; line = rdr.readLine()) {
                lines.add(line);
            }
            return lines;
        } catch (IOException ex) {
        }
        return lines;
    }
}
