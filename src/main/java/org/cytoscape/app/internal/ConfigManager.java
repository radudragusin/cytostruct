package org.cytoscape.app.internal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.cytoscape.app.CyAppAdapter;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.session.CySession;
import org.cytoscape.session.events.SessionAboutToBeSavedEvent;
import org.cytoscape.session.events.SessionAboutToBeSavedListener;
import org.cytoscape.session.events.SessionLoadedEvent;
import org.cytoscape.session.events.SessionLoadedListener;
import org.osgi.framework.BundleContext;

public class ConfigManager implements SessionAboutToBeSavedListener, SessionLoadedListener {

    private BundleContext bc;
    private CyAppAdapter adapter;
    private CyActivator activator;
    private File sessionDir;

    public CyApplicationManager cyApplicationManager;

	//HashMap<Integer,EdgeInfo> edgesDB;
    public final static String DATA_ROOT_PROP_NAME = "R4M_DATA_PATH";

    private boolean loaded;
    private boolean store;
    private ArrayList<String> appsRawLines;

    public ConfigManager(CyApplicationManager cyApplicationManager,
            BundleContext bc,
            CyAppAdapter adapter,
            CyActivator activator) {
        this.cyApplicationManager = cyApplicationManager;
        this.bc = bc;
        this.adapter = adapter;
        this.activator = activator;
        this.loaded = false;
        this.store = false;
        this.appsRawLines = new ArrayList<String>();
    }

    public String getDir() { return this.sessionDir!=null ? this.sessionDir.getAbsolutePath() : ""; }
    public void setDir(String sessionName) {
        sessionDir = new File(sessionName).getParentFile().getAbsoluteFile();
        cdir();
    }
    
    public void cdir() {
        if(sessionDir != null)
            cdir(sessionDir);
    }
    
    public boolean isLoaded() { return loaded; }
    private void cleanStore() { this.store = false; }
    public boolean isStore() { return this.store; }
    public ArrayList<String> getApps() {
        return appsRawLines;
    }

    public void setApps(ArrayList<String> appRaw, boolean registerHandlers) {
        this.appsRawLines = appRaw;
        this.store = true;
        this.loaded = true;
        if(registerHandlers)
            activator.registerNodeEdgeHandlers(appRaw,true);
    }
    
    // Save app state in a file
    @Override
    public void handleEvent(SessionAboutToBeSavedEvent e) {
        if (!this.store) 
            return;
        if (this.appsRawLines == null || this.appsRawLines.isEmpty()) 
            return;

        String tmpDir = System.getProperty("java.io.tmpdir");
        File propFile = new File(tmpDir, DATA_ROOT_PROP_NAME);
        
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(propFile));
            for(String line : this.appsRawLines) {
                writer.write(line);
                writer.write(System.getProperty( "line.separator" ));
            }
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            ErrBuffer.err("<Debug> 1: Couldn't write path " + propFile.getAbsolutePath());
        }

        ArrayList<File> files = new ArrayList<File>();
        files.add(propFile);
        try {
            e.addAppFiles(DATA_ROOT_PROP_NAME, files);
        } catch (Exception ex) {
            ErrBuffer.err("<Debug> 2: Couldn't add file to session " + propFile.getAbsolutePath());
        }
        this.store = false;
    }

    private void usePropFile(File propFile) {
        if (propFile != null && propFile.exists()) {
            ErrBuffer.out("using prop @ " + propFile.getAbsolutePath());
            this.appsRawLines = Utils.readAllLines(propFile);
            this.loaded = !this.appsRawLines.isEmpty();
            //while at this -- try and get the data if possible
        } 
    }
    
    protected File guessFileWithHack(Map<String, List<File>> mapF) {
        String anyS = mapF.keySet().iterator().next();
        File anyFile = mapF.get(anyS).get(0);
        File propF = new File(anyFile.getParentFile().getAbsoluteFile() + File.separator + DATA_ROOT_PROP_NAME);
        return propF;
    }
    
    public boolean getDataFromOpenSession(CySession s) {
        List<File> files = s.getAppFileListMap().get(DATA_ROOT_PROP_NAME);
        if (files != null && files.size() > 0) {
            ErrBuffer.out("getDataFromOpenSession - main");
            usePropFile(files.get(0));             //let's try to read the cfg	
            return this.loaded;
        }
        //let's try the hack...
        ErrBuffer.out("getDataFromOpenSession - hack");
        File propF = guessFileWithHack(s.getAppFileListMap());
        usePropFile(propF);      
        return this.loaded;
    }
    
    protected void cdir(File directory) {
        ErrBuffer.out("trying to cd to " + directory.getAbsolutePath());
        System.setProperty("user.dir", directory.getAbsolutePath());
    }
    
    protected File cdir(String loadedFname) {
        File fcys = new File(loadedFname);
        String candidatePath = fcys.getParentFile().getAbsoluteFile() + File.separator;
        File directory = new File(candidatePath).getAbsoluteFile();
        return directory;
    }
	// restore app state from a file
    //DO NOT OptionPane.showMessageDialog in this one! 
    @Override
    public void handleEvent(SessionLoadedEvent e) {
        /*
        File propFile = null;
        //First see if we have an override:
        if (e.getLoadedSession().getAppFileListMap() != null&& !e.getLoadedSession().getAppFileListMap().isEmpty()) {
            if(! getDataFromOpenSession(e.getLoadedSession())) { //no cfg saved in this session
                //see if there is an apps file in local path
                File fcys = new File(e.getLoadedFileName());
                String candidatePath = fcys.getParentFile().getAbsoluteFile() + File.separator;

                File configData = new File(candidatePath + DATA_ROOT_PROP_NAME);
                if (configData.exists()) { //we have them in default location
                    propFile = configData;
                }
                usePropFile(propFile); //give it a try if not null
            }
            if(this.loaded) {
                activator.registerNodeEdgeHandlers(appsRawLines,true);
           }

        }  
                */
        cleanStore();
        sessionDir = cdir(e.getLoadedFileName());
        activator.stopStart();
    }
   
    
}
