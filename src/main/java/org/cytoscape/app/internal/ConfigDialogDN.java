package org.cytoscape.app.internal;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ConfigDialogDN extends JFrame {

    private ConfigManager targetCfg;
    private JTextArea tApps;

    public ConfigDialogDN(ConfigManager targetCfg) {
        super("Configure");
        this.targetCfg = targetCfg;

        setSize(new Dimension(120, 40));
        JPanel cfgPanel = new JPanel();
        cfgPanel.setLayout(new BoxLayout(cfgPanel, BoxLayout.Y_AXIS));

        cfgPanel.add(new JLabel("\n\n Define your applications in a valid YAML: "));
        tApps = new JTextArea(20, 40);
        tApps.setFont(new Font("Courier", Font.PLAIN, 18));
        tApps.setText(Utils.joinArrayString(targetCfg.getApps()));
        tApps.setEditable(true);
        cfgPanel.add(new JScrollPane(tApps));
        JButton b1 = new JButton("Or load from file");
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) { handleLoadFromFile(); }});
        cfgPanel.add(b1);

        JButton bs = new JButton("Set configuration (will be also stored in .cys when saving the session)");
        bs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                handleSave();
            }
        });
        cfgPanel.add(bs);

        getContentPane().add(cfgPanel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    
     private void handleLoadFromFile() {
         JFileChooser chooser = new JFileChooser();
         chooser.setDialogTitle("Select applications YAML");
         if (chooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) 
            return;
         this.tApps.setText(Utils.joinArrayString(Utils.readAllLines(chooser.getSelectedFile())));
     }
  
    private void handleSave() {
        setVisible(false);
        dispose(); //Destroy the JFrame object
        targetCfg.setApps(Utils.StringToLines(tApps.getText()),true);
    }

}
